import React from 'react';


function ShoesList(props) {

  // function sayHello() {
  //   console.log("hello");
  // }

  // removeItem(e) {
  //   this.props.removeShoe(shoe);
  // }


  return (
    <table className="table table-striped">
        <thead>
          <tr>
            <th>Shoe Name</th>
            <th>Manufacture</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
            {/* <th>Delete</th> */}
          </tr>
        </thead>
        <tbody>
          {props.shoes.map(shoe => {
            return (
              <tr  key={shoe.id}>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.color }</td>
                <td><img src={shoe.picture_url} height={100} width={100}/></td>
                <td>{ shoe.bin.closet_name }</td>
                {/* <button onClick={sayHello}> */}
                  {/* Click me! */}
                {/* </button> */}
              </tr>
            );
          })}
        </tbody>
    </table>
  )
}

export default ShoesList;