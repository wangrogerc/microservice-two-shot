import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

function HatsList(props){
    return(
        <table>
          <thead>
            <tr>
              <th>Style</th>
              <th>Color</th>
              <th>Fabric</th>
            </tr>
          </thead>
          <tbody>
            {props.hats.map(hat => {
            return (
                
              <tr key={hat.id}>
                <td>{hat.style}</td>
                <td>{hat.color}</td>
                <td>{hat.fabric}</td>
                <td><img src={hat.hat_url} height={100} width={100}/></td>
                <td>{hat.location.closet_name}</td>
              </tr>
              
            );
            })}
          </tbody>
        </table>)
  }
  
  export default HatsList
  