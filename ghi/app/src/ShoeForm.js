import React from 'react';


class ShoeForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
     modelName: '',
     manufacturer: '',
     color: '',
     pictureUrl: '',
     bins: []
    }

    this.handleModelNameChange = this.handleModelNameChange.bind(this);
    this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handlePictureChange = this.handlePictureChange.bind(this);
    this.handleBinChange = this.handleBinChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleModelNameChange(event) {
    const value = event.target.value;
    this.setState({modelName: value})
  }

  handleManufacturerChange(event) {
    const value = event.target.value;
    this.setState({manufacturer: value})
  }

  handleColorChange(event) {
    const value = event.target.value;
    this.setState({color: value})
  }

  handlePictureChange(event) {
    const value = event.target.value;
    this.setState({pictureUrl: value})
  }

  handleBinChange(event) {
    const value = event.target.value;
    this.setState({bin: value})
  }

  async handleSubmit(event) {
    event.preventDefault()
    const data = {...this.state}
    console.log(data);
    data.model_name = data.modelName
    data.picture_url = data.pictureUrl
    delete data.pictureUrl
    delete data.modelName
    delete data.bins

    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
          'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoeUrl, fetchConfig);

    if (response.ok) {
      const newShoe = await response.json();
      console.log("New Shoe data: ",newShoe);
      const cleared = {
        modelName: '',
        manufacturer: '',
        color: '',
        pictureUrl: '',
        bins: []
      };
      this.setState(cleared);
    }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/bins/';

    const response = await fetch(url);
    // console.log(response);

    if (response.ok) {
        const data = await response.json();
        console.log(data);
        this.setState({bins: data.bins})
    }
  }


  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={this.handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleModelNameChange} placeholder="Model name" required type="text"  value={this.state.modelName} name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="model_name">
                Model name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleManufacturerChange} placeholder="Manufacturer" required  type="text" value={this.state.manufacturer} name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleColorChange} placeholder="Color" required  type="text"  value={this.state.color} name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handlePictureChange} placeholder="Picture url" required  type="text"  value={this.state.pictureUrl} name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture url</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleBinChange} required id="bin" value={this.state.bin} name="bin" className="form-select">
                    <option value="">Choose a bin</option>
                    {this.state.bins.map(bin => {
                      return (
                        <option  value={bin.href} key={bin.href}>
                          {bin.closet_name}
                        </option>
                      );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ShoeForm;