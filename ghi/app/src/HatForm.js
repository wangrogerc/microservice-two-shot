import React from "react";
import { useForm } from "react-hook-form";

export default function HatForm(props){
    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const onSubmit = data => console.log(data);
  
    console.log(watch("example")); // watch input value by passing the name of it
  
    return (
      /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
      <form onSubmit={handleSubmit(onSubmit)}>
        {/* register your input into the hook by invoking the "register" function */}
        <input defaultValue="test" {...register("example")} />
        
        {/* include validation with required or other standard HTML validation rules */}
        <input {...register("exampleRequired", { required: true })} />
        {/* errors will return when field validation fails  */}
        {errors.exampleRequired && <span>This field is required</span>}
               <select id="state" className="form-select">
         <option {...register("location", {required:true})} value="">Choose a location</option>
                         {props.hats.map(hat => {
                        return (
                            <option key = {hat.location.id} value ={hat.location.import_href}>
                                {hat.location.closet_name}
                           </option>)})} 
       </select>
        <input type="submit" />
      </form>
    );
  }








// const { register, handleSubmit, watch, reset, formState: { errors } } = useForm();
//  const onSubmit = async function(data){
//     console.log("wat")
//     const hatsUrl = 'http://localhost:8000/api/locations/';
//     const fetchConfig = {
//         method: "post",
//         body: JSON.stringify(data), 
//         headers: {
//             'Content-Type': 'application/json',
//         }
//     }
//     const response = await fetch(hatsUrl, fetchConfig);
//     if (response.ok) {
//         console.log("okayed")
//         const newLocation = await response.json();
//         console.log(newLocation)
//         reset()
// }
//  }
// // const onSubmit = data => console.log(data);
  
//     console.log(watch("example")); // watch input value by passing the name of it


// return (
//     /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
//     <form onSubmit={handleSubmit(onSubmit)}>
//       {/* register your input into the hook by invoking the "register" function */}
//       <input  {...register("example", {required: true})} />
      
//       {/* include validation with required or other standard HTML validation rules */}
//       <input {...register("exampleRequired", { required: true })} />
//       <select id="state" className="form-select">
//         <option {...register("location", {required:true})} value="">Choose a location</option>
//                         {props.hats.map(hat => {
//                         return (
//                             <option key = {hat.location.id} value ={hat.location.import_href}>
//                                 {hat.location.closet_name}
//                             </option>)})} 
//         </select>

//         {errors.exampleRequired && <span>This field is required</span>}
      
//         <button className="btn btn-primary">Create</button>
//     </form>
//   );
// }