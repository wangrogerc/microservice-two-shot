import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import HatsList from './HatsList';
import Nav from './Nav';
import Nav2 from './Nav2'
import HatForm from './HatForm';
import ShoesList from './ShoesList';
import ShoeForm from "./ShoeForm";

function App(props) {




  // constructor(props) {
  //   super(props)

  //   this.removeShoe = this.removeShoe.bind(this);
  // }

  // removeShoe(id){
  //   this.setState({
  //       shoes: this.state.shoes.filter(el => el !== id)
  //   });
  // }

  if (props.shoes === undefined && props.hats == undefined) {
    return null;
  }
  return (
    <>
    <BrowserRouter>
      <Nav />
      <Nav2 />
      <div className="container">
        <Routes>
          <Route>
          <Route path="/" element={<MainPage />} />
          </Route>
          <Route>
          <Route path="hats" element={<HatsList hats={props.hats} />}  />
          </Route>
          <Route>
          <Route path="create-hat" element={<HatForm hats={props.hats}/>} />
          </Route>
          <Route>
          <Route path="shoes" element={<ShoesList shoes={props.shoes} />}/>
          </Route>
          <Route path="shoes">
            <Route path="new" element={<ShoeForm />}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
