from cmath import log
from django.shortcuts import render
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO

class BinVOEncoder(ModelEncoder):
  model = BinVO
  properties = [
    "closet_name",
    "import_href"
  ]

class ShoeListEncoder(ModelEncoder):
  model = Shoe
  properties = [
    "id",
    "model_name",
    "manufacturer",
    "color",
    "picture_url",
    "bin"
  ]
  encoders = {
      "bin": BinVOEncoder(),
  }



class ShoeDetailEncoder(ModelEncoder):
  model = Shoe
  properties = [
    "model_name",
    "manufacturer",
    "color",
    "picture_url",
     "bin"
  ]
  encoders = {
    "bin": BinVOEncoder(),
  }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request,  bin_vo_id=None):
  if request.method == "GET":
    if bin_vo_id is not None:
        shoes = Shoe.objects.filter(bin=bin_vo_id)
    else:
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
  else:
      content = json.loads(request.body)

      try:
        bin_href = content["bin"]
        bin = BinVO.objects.get(import_href=bin_href)
        content["bin"] = bin
      except BinVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid bin id"},
            status=400,
        )

      shoe = Shoe.objects.create(**content)
      return JsonResponse(
          shoe,
          encoder=ShoeDetailEncoder,
          safe=False,
      )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
  if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
  elif request.method == "DELETE":
    count, _ = Shoe.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})
  else:
      content = json.loads(request.body)
      Shoe.objects.filter(id=pk).update(**content)
      shoe = Shoe.objects.get(id=pk)
      return JsonResponse(
          shoe,
          encoder=ShoeDetailEncoder,
          safe=False,
      )
