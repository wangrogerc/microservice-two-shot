from django.db import models

# Create your models here.

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, unique=True)


class Hat(models.Model):
    fabric = models.CharField(max_length=75)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    hat_url = models.URLField()
    location = models.ForeignKey(LocationVO, related_name="hats", on_delete=models.CASCADE, null=True)


