from django.shortcuts import render
from .models import Hat, LocationVO
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href"]

class HatEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style", "color", "hat_url", "location"]

    encoders = {"location": LocationVODetailEncoder()}
    # def get_extra_data(self, o):
        # return {"location": o.location.import_href}

@require_http_methods(["GET", "POST"])
def api_hats(request, location_vo_id = None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatEncoder)
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(hat, encoder=HatEncoder, safe=False)

@require_http_methods(["DELETE", "GET", "PUT"])
def api_hat(request, pk):
    if request.method == "GET":
        try: 
            hat = Hat.objects.get(id=pk)
            return JsonResponse(hat, encoder=HatEncoder, safe=False)
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method =="DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(hat, encoder=HatEncoder, safe=False)
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            hat = Hat.objects.get(id=pk)
            props = ["fabric", "style", "color"]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(hat, encoder= HatEncoder, safe=False)
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    
