# Wardrobify

Team:

* Person 1 - Which microservice?
* Roger -Hats
* Ethan - Shoes

## Design

## Shoes microservice
Start in shoes rest and create models
Then move on to making views based on the model
Set up url to test in Insomnia
TO Be continued...


You need to install your Django app into the Django project for your microservice. That's the INSTALLED_APPS deal.

Then, try making the simplest model possible, not worrying about integration until later.

Make one function view to show the list of your model. Whenever you need a point of reference about how to do something for the RESTful APIs, look at your code in Conference GO! or the Wardrobe API in the project.

Configure the view in a URLs file that you will have to create for your Django app.

Include the URLs from your Django app into your Django project's URLs.

See if you can use Insomnia to see an empty list of your resources.

Follow the steps for Git to get your code in the main branch based on Git In a Group.

Add to the function to handle POST requests to create a new record in your database. Use Insomnia to see if you can POST data to create a record. Then, use the GET to see if it's now in the list.

Follow the steps for Git to get your code in the main branch based on Git In a Group.

Now, try to build a React component to fetch the list and show the list of your resources, hats or shoes, in the Web page. Try to make it pretty, if you'd like. The React app has Bootstrap already in it. Route your list component to the appropriate path to show up when you click on the navigation bar link. This is very much like either the AttendeesList component or the MainPage component from Conference GO! But, you will make your own list component, like ShoeList or HatList and use that.

Follow the steps for Git to get your code in the main branch based on Git In a Group.

Now, keep adding little pieces of functionality. Each time you succeed at something, follow the steps for Git to get your code in the main branch based on Git In a Group. If you're doing the dev branch strategy, merge your working code to main. Keep going until you can successfully add, view, and delete your resource using the React UI using your microservice's back-end.

At some point, write your poller to get the Bin or Location data for your microservice. Use the requests library to make the HTTP request to the Wardrobe API, then loop over it and add it to your database. This is like when you polled for Account information from the attendees microservice in Conference GO!

Leave the "delete" functionality for last. Then, add a delete button for each item in your list. If you give that button an onClick handler, you can use that to make a fetch with the HTTP method DELETE. You'll just need to figure out how to get the id of the thing you want to delete for the URL. To solve this, you could do something like this for the onClick handler, assuming you have a variable named hat that contains the Hat data.

<button onClick={() => this.delete(hat.id)}>

The existing Wardrobe API can be accessed from your polling service on port 8000. Your service's poller will poll the base URL http://wardrobe-api:8000.

It has full RESTful APIs to interact with Bin and Location resources.

Method	URL	What it does
GET	/api/bins/	Gets a list of all of the bins
GET	/api/bins/<int:pk>/	Gets the details of one bin
POST	/api/bins/	Creates a new bin with the posted data
PUT	/api/bins/<int:pk>/	Updates the details of one bin
DELETE	/api/bins/<int:pk>/	Deletes a single bin
Method	URL	What it does
GET	/api/locations/	Gets a list of all of the locations
GET	/api/locations/<int:pk>/	Gets the details of one location
POST	/api/locations/	Creates a new location with the posted data
PUT	/api/locations/<int:pk>/	Updates the details of one location
DELETE	/api/locations/<int:pk>/	Deletes a single location

## Hats microservice

Going to write backend first, then APIs, then front end. 
